// Package gopher provides implementations for Gopher clients and servers.
//
// Fetch can be used to make Gopher requests:
//
//     reader, err := gopher.Fetch("example.com", "/hello")
//     // ...
//
// The reader must be closed after it's been used:
//
//     reader, err := gopher.Fetch("example.com", "/hello")
//     if err != nil {
//         // handle error
//     }
//     defer reader.Close()
//     // ...
//
// For more control over requests, create a Client:
//
//     client := gopher.Client{
//         Timeout: time.Second * 30
//     }
//     
//     reader, err := client.Fetch("example.com", "/")
//     // ...
//
// ListenAndServe starts a Gopher server with a given address and handler. If
// handler is nil, DefaultServeMux will be used. Handle and HandleFunc can be
// used to add handlers to DefaultServeMux.
//
//     gopher.Handle("/foo", fooHandler)
//
//     gopher.HandleFunc("/bar", func (w io.Writer, req *gopher.Request) {
//         w.Write([]byte("Hello, world!\r\n.\r\n"))
//     })
//
//     log.Fatal(gopher.ListenAndServe(":7070", nil))
//
// For more control over the server, create a Server:
//
//     s := &http.Server{
//         Addr: ":7070",
//         Handler: fooHandler,
//         ReadTimeout: 30 * time.Second,
//         WriteTimeout: 30 * time.Second,
//         MaxPathBytes: 64,
//     }
//     log.Fatal(s.ListenAndServe())
package gopher

import (
	"io"
)

// readLine reads a line terminated by CRLF.
func readLine(r io.Reader, l int) (string, error) {
	var c [1]byte
	var buf []byte
	for l != 0 {
		_, err := r.Read(c[:])
		if err != nil {
			return "", err
		}

		if c[0] == '\r' {
			_, err := r.Read(c[:])
			if err != nil {
				return "", err
			}

			if c[0] == '\n' {
				return string(buf), nil
			}
			buf = append(buf, '\r')
			l--
		}
		buf = append(buf, c[0])
		l--
	}

	return "", errLineTooLong
}
