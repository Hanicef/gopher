package gopher

import (
	"io"
	"net"
	"strings"
	"time"
)

type Client struct {
	// Timeout specifies a time limit for the entire request. This includes
	// request and response, but not connection time. Note that the timer
	// remains after the returning from Fetch and List, and will interrupt
	// reading from the returned reader.
	//
	// A Timeout of zero means no timeout.
	Timeout time.Duration
}

// Fetch opens a Gopher connection and fetches the content in the specified
// path. The returned reader should be closed by the caller when done reading.
func (c *Client) Fetch(host, path string) (io.ReadCloser, error) {
	if !strings.Contains(host, ":") {
		host = host + ":70"
	}

	conn, err := net.Dial("tcp", host)
	if err != nil {
		return nil, err
	}

	if c.Timeout != time.Duration(0) {
		err = conn.SetDeadline(time.Now().Add(c.Timeout))
		if err != nil {
			conn.Close()
			return nil, err
		}
	}

	_, err = conn.Write([]byte(path + "\r\n"))
	if err != nil {
		conn.Close()
		return nil, err
	}

	return conn, nil
}

// Fetch opens a Gopher connection and fetches the content in the specified
// path. The returned reader should be closed by the caller when done reading.
//
// Fetch is a wrapper around Client.Fetch.
func Fetch(host, path string) (io.ReadCloser, error) {
	client := Client{}
	return client.Fetch(host, path)
}
