package gopher

import (
	"io"
	"strings"
	"testing"
)

func TestServeMux(t *testing.T) {
	mux := ServeMux{}
	mux.HandleFunc("/", func (writer io.Writer, req *Request) {
		writer.Write([]byte("path " + req.Path + " not found"))
	})
	mux.HandleFunc("/hello", func (writer io.Writer, req *Request) {
		writer.Write([]byte("hello, world!"))
	})
	mux.HandleFunc("/hello/", func (writer io.Writer, req *Request) {
		writer.Write([]byte("hello, " + req.Path[7:] + "!"))
	})
	mux.HandleFunc("hello", func (writer io.Writer, req *Request) {
		writer.Write([]byte("hi, world!"))
	})
	mux.HandleFunc("hello/", func (writer io.Writer, req *Request) {
		writer.Write([]byte("hi, " + req.Path[6:] + "!"))
	})
	mux.HandleFunc("/goodbye/", func (writer io.Writer, req *Request) {
		if req.Path == "/goodbye" {
			writer.Write([]byte("goodbye, world!"))
			return
		}
		writer.Write([]byte("goodbye, " + req.Path[9:] + "!"))
	})

	b := strings.Builder{}
	mux.ServeGopher(&b, &Request{Path: "/foo"})
	if b.String() != "path /foo not found" {
		t.Errorf("ServeMux failed: b.String() != \"path /foo not found\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "/"})
	if b.String() != "path / not found" {
		t.Errorf("ServeMux failed: b.String() != \"path / not found\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "/hello"})
	if b.String() != "hello, world!" {
		t.Errorf("ServeMux failed: b.String() != \"hello, world!\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "/hello/steve"})
	if b.String() != "hello, steve!" {
		t.Errorf("ServeMux failed: b.String() != \"hello, steve!\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "/goodbye"})
	if b.String() != "goodbye, world!" {
		t.Errorf("ServeMux failed: b.String() != \"goodbye, world!\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "/goodbye/steve"})
	if b.String() != "goodbye, steve!" {
		t.Errorf("ServeMux failed: b.String() != \"goodbye, steve!\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "hello"})
	if b.String() != "hi, world!" {
		t.Errorf("ServeMux failed: b.String() != \"hi, world!\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "hello/steve"})
	if b.String() != "hi, steve!" {
		t.Errorf("ServeMux failed: b.String() != \"hi, steve!\" (%s)", b.String())
	}

	b.Reset()
	mux.ServeGopher(&b, &Request{Path: "hello?steve"})
	if b.String() != "hi, world!" {
		t.Errorf("ServeMux failed: b.String() != \"hi, world!\" (%s)", b.String())
	}
}
