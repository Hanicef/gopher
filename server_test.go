package gopher

import (
	"context"
	"io"
	"io/ioutil"
	"net"
	"testing"
)

func TestServer(t *testing.T) {
	server := Server{
		Addr: "localhost:7070",
		Handler: HandlerFunc(func (writer io.Writer, req *Request) {
			writer.Write([]byte("you fetched " + req.Path))
		}),
		MaxPathBytes: 16,
	}
	go server.ListenAndServe()
	defer server.Shutdown()

	r, err := Fetch("localhost:7070", "/")
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	data, err := ioutil.ReadAll(r)
	r.Close()
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	if string(data) != "you fetched /" {
		t.Errorf("ListenAndServe() failed: string(data) != \"you fetched /\" (%s)", string(data))
	}

	r, err = Fetch("localhost:7070", "/hello")
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	data, err = ioutil.ReadAll(r)
	r.Close()
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	if string(data) != "you fetched /hello" {
		t.Errorf("ListenAndServe() failed: string(data) != \"you fetched /hello\" (%s)", string(data))
	}

	r, err = Fetch("localhost:7070", "/this/path/is/too/long")
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	defer r.Close()
	items := NewItemReader(r)
	i, err := items.Next()
	if err != nil {
		t.Fatal("Next() failed:", err)
	}
	if i.Type != ItemError {
		t.Errorf("ListenAndServe() failed: i.Type != ItemError (%d)", i.Type)
	} else if i.Name != "error: line too long" {
		t.Errorf("ListenAndServe() failed: i.Name != \"error: line too long\" (%s)", i.Name)
	}
}

func TestRequest_Context(t *testing.T) {
	server := Server{
		Addr: "localhost:7070",
		Handler: HandlerFunc(func (writer io.Writer, req *Request) {
			ctx := req.Context()
			writer.Write([]byte("addr: " + ctx.Value(LocalAddrContextKey).(net.Addr).String()))
			ctx.Value("cancel").(context.CancelFunc)()
			<-ctx.Done()
		}),
		ConnContext: func (ctx context.Context, conn net.Conn) context.Context {
			var c context.CancelFunc
			ctx, c = context.WithCancel(ctx)
			return context.WithValue(ctx, "cancel", c)
		},
	}
	go server.ListenAndServe()
	defer server.Shutdown()

	r, err := Fetch("localhost:7070", "")
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	data, err := ioutil.ReadAll(r)
	r.Close()
	if err != nil {
		t.Fatal("Fetch() failed:", err)
	}
	if string(data) != "addr: 127.0.0.1:7070" {
		t.Errorf("ConnContext failed: string(data) != \"addr: 127.0.0.1:7070\" (%s)", string(data))
	}
}
