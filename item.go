package gopher

import (
	"errors"
	"io"
	"strings"
)

// An Item contains data for a Gopher item.
type Item struct {
	// Type is the item type. Other fields may be formatted differently
	// depending on Type; see the item constants for more details.
	Type byte

	// Name is the item name that is normally displayed by clients.
	Name string

	// Host is the host where the corresponding item is located, in the form
	// "host:port". This can safely be passed directly to Fetch.
	Host string

	// Path is the path to the item at the server located at Host.
	Path string
}

// Item types, as defined by RFC 1436. Some are non-standard and changes the
// meaning of some fields in Item.
const (
	ItemFile = '0'
	ItemDirectory = '1'
	ItemCSONameServer = '2'
	ItemError = '3'
	ItemBinHex = '4'
	ItemDOSFile = '5' // Content may not end with ".\r\n"
	ItemUuencoded = '6'
	ItemSearch = '7'
	ItemTelnet = '8' // Item.Path is ignored
	ItemBinary = '9' // Content may not end with ".\r\n"
	ItemMirror = '+'
	ItemTelnet3270 = 'T' // Item.Path is ignored
	ItemGIF = 'g' // Content may not end with ".\r\n"
	ItemImage = 'I' // Content may not end with ".\r\n"
	ItemDocument = 'd' // Non-standard
	ItemHTML = 'h' // Non-standard; Item.Host is ignored; Item.Path is a URL prefixed by "URL:"
	ItemInformation = 'i' // Non-standard; Item.Host and Item.Path is ignored
	ItemSound = 's' // Non-standard; content may not end with ".\r\n"
)

var errLineTooLong = errors.New("line too long")

// String returns the string representation of Item. Can safely be passed
// directly to a Handler's io.Writer.
func (i *Item) String() string {
	var buf = make([]byte, 0, 5 + len(i.Name) + len(i.Path) + len(i.Host))
	buf = append(buf, i.Type)
	buf = append(buf, i.Name...)
	buf = append(buf, '\t')
	buf = append(buf, i.Path...)
	buf = append(buf, '\t')
	buf = append(buf, strings.Replace(i.Host, ":", "\t", 1)...)
	buf = append(buf, '\r', '\n')
	return string(buf)
}

// ErrMalformattedItem is returned when an incorrectly formatted item is read
// from an ItemReader.
var ErrMalformattedItem error = errors.New("gopher: malformatted item in ItemReader")

// An ItemReader reads Items from an io.Reader.
type ItemReader struct {
	r io.Reader
	eof bool
	l string
}

// NewItemReader returns a new ItemReader reading from r.
func NewItemReader(r io.Reader) *ItemReader {
	return &ItemReader{
		r: r,
		eof: false,
	}
}

// ErrorLine returns the line that caused the ErrMalformattedLine to be
// returned from Next. If no malformatted line has been encountered, ErrorLine
// returns an empty string.
func (ir *ItemReader) ErrorLine() string {
	return ir.l + "\r\n"
}

// Next reads the next Item from the reader. If the next item is malformatted,
// Next returns nil, ErrMalformattedItem.
//
// Due to how Gopher is designed, it's impossible for a client to know ahead of
// time what format to expect for arbitrary hosts and paths. Unless the format
// is known to be correct ahead of time, a return value of ErrMalformattedItem
// can be recovered by calling String on all previously read Items as well as a
// call to ErrorLine.
func (ir *ItemReader) Next() (*Item, error) {
	if ir.r == nil {
		panic("gopher: attempt to read item from zero value")
	}

	if ir.eof {
		return nil, io.EOF
	}

	line, err := readLine(ir.r, -1)
	if err != nil {
		if err == io.EOF {
			ir.eof = true
			ir.l = line
			return nil, ErrMalformattedItem
		}
		return nil, err
	}
	if len(line) == 0 {
		ir.l = line
		return nil, ErrMalformattedItem
	}

	if len(line) == 1 && line[0] == '.' {
		ir.eof = true
		return nil, io.EOF
	}

	split := strings.Split(line[1:], "\t")
	if len(split) != 4 {
		ir.l = line
		return nil, ErrMalformattedItem
	}

	i := new(Item)
	i.Type = line[0]
	i.Name = split[0]
	i.Host = split[2] + ":" + split[3]
	i.Path = split[1]
	return i, nil
}
