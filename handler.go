package gopher

import (
	"errors"
	"io"
	"net/http"
	"path"
	"time"
)

// A Handler responds to a Gopher request.
//
// ServeGopher should write the response data to the supplied io.Writer and
// then return. Returning signals that the request is finished; it is not valid
// to use the io.Writer after returning from ServeGopher.
//
// Text content must end each line with "\r\n", and send ".\r\n" before
// returning from ServeGopher. Item entries has a more strict format, see Item
// and RFC 1436 for more details.
type Handler interface {
	ServeGopher(io.Writer, *Request)
}

// The HandlerFunc type is an adapter to allow the use of ordinary functions as
// Gopher handlers. If f is a function with appropriate signature,
// HandlerFunc(f) is a Handler that calls f.
type HandlerFunc func (io.Writer, *Request)

type timeoutWriter struct {
	buf      []byte
	timedOut bool
}

type timeoutHandler struct {
	handler Handler
	timeout time.Duration
	msg     []byte
}

type fileSystemHandler struct {
	host string
	root http.FileSystem
}

// ErrHandlerTimeout is returned on io.Writer Write calls in handlers which
// have timed out.
var ErrHandlerTimeout = errors.New("gopher: Handler timeout")

// ServeGopher calls h(w, req)
func (h HandlerFunc) ServeGopher(w io.Writer, req *Request) {
	h(w, req)
}

func (writer *timeoutWriter) Write(p []byte) (int, error) {
	if writer.timedOut {
		return 0, ErrHandlerTimeout
	}
	writer.buf = append(writer.buf, p...)
	return len(p), nil
}

func (handler *timeoutHandler) ServeGopher(writer io.Writer, req *Request) {
	statusChan := make(chan bool)
	timeout := timeoutWriter{}
	timer := time.AfterFunc(handler.timeout, func () {
		timeout.timedOut = true
		statusChan <- false
	})
	go func () {
		handler.handler.ServeGopher(&timeout, req)
		statusChan <- true
	}()
	switch <-statusChan {
	case true:
		if !timer.Stop() {
			<-timer.C
			<-statusChan // Wait for timer
		}
		writer.Write(timeout.buf)
	case false:
		writer.Write(handler.msg)
		<-statusChan // Wait for handler
	}
}

// TimeoutHandler returns a Handler that runs h with a given time limit.
//
// The new Handler calls h.ServeGopher to handle each request, but if a call
// runs for longer than its time limit, the handler responds with msg. After
// such a timeout, writes by h to its io.Writer will return ErrHandlerTimeout.
func TimeoutHandler(h Handler, dt time.Duration, msg string) Handler {
	handler := new(timeoutHandler)
	handler.handler = h
	handler.timeout = dt
	handler.msg = []byte(msg)
	return handler
}

func (fs *fileSystemHandler) ServeGopher(writer io.Writer, req *Request) {
	ctx := req.Context()
	srv := ctx.Value(ServerContextKey).(*Server)
	file, err := fs.root.Open(req.Path)
	if err != nil {
		srv.log(err)
		writeError(writer, err)
		return
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		srv.log(err)
		writeError(writer, err)
		return
	}
	if info.IsDir() {
		files, err := file.Readdir(0)
		if err != nil {
			srv.log(err)
			writeError(writer, err)
			return
		}
		for _, f := range files{
			i := Item{
				Name: f.Name(),
				Host: fs.host,
				Path: path.Join(req.Path, f.Name()),
			}
			if f.IsDir() {
				i.Type = ItemDirectory
			} else {
				i.Type = ItemFile
			}
			_, err = writer.Write([]byte(i.String()))
			if err != nil {
				srv.log(err)
				return
			}
		}
		_, err = writer.Write([]byte(".\r\n"))
		if err != nil {
			srv.log(err)
			return
		}
	} else {
		io.Copy(writer, file)
	}
}

// FileServer returns a Handler that serves Gopher requests with the contents
// of the file system rooted at root.
//
// To use the operating system's file system implementation, use http.Dir:
//
//     gopher.Handle("/", gopher.FileServer(http.Dir("/tmp")))
func FileServer(host string, root http.FileSystem) Handler {
	fs := new(fileSystemHandler)
	fs.host = host
	fs.root = root
	return fs
}
