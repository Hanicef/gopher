package gopher

import (
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"
)

type TimeoutTest time.Duration

func (t TimeoutTest) ServeGopher(writer io.Writer, req *Request) {
	time.Sleep(time.Duration(t))
	writer.Write([]byte("done"))
	return
}

func TestTimeoutHandler(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}
	handler := TimeoutHandler(TimeoutTest(time.Millisecond * 200), time.Millisecond * 500, "timed out")
	builder := strings.Builder{}
	handler.ServeGopher(&builder, &Request{Path: ""})
	if builder.String() != "done" {
		t.Errorf("TimeoutHandler() failed: builder.String() != \"done\" (%s)", builder.String())
	}

	handler = TimeoutHandler(TimeoutTest(time.Millisecond * 200), time.Millisecond * 100, "timed out")
	builder.Reset()
	handler.ServeGopher(&builder, &Request{Path: ""})
	if builder.String() != "timed out" {
		t.Errorf("TimeoutHandler() failed: builder.String() != \"timed out\" (%s)", builder.String())
	}
}

func TestFileServer(t *testing.T) {
	var testItem = func (item *Item, typ byte, name, host, path string) {
		t.Log("Verifying ", name)
		if item.Type != typ {
			t.Errorf("FileServer() failed: item.Type != typ (%c != %c)", item.Type, typ)
		}
		if item.Name != name {
			t.Errorf("FileServer() failed: item.Name != name (%s != %s)", item.Name, name)
		}
		if item.Host != host {
			t.Errorf("FileServer() failed: item.Host != host (%s != %s)", item.Host, host)
		}
		if item.Path != path {
			t.Errorf("FileServer() failed: item.Path != Path (%s != %s)", item.Path, path)
		}
	}
	var fooData = []byte("goodbye, world!\r\n.\r\n")
	var barData = []byte("hello, world!\r\n.\r\n")
	err := os.Mkdir("/tmp/__go_gopher_test_fs", os.FileMode(0755))
	if err != nil && !os.IsExist(err) {
		t.Fatal("os.Mkdir() failed:", err)
	}

	err = os.Mkdir("/tmp/__go_gopher_test_fs/foo", os.FileMode(0755))
	if err != nil && !os.IsExist(err) {
		t.Fatal("os.Mkdir() failed:", err)
	}

	err = ioutil.WriteFile("/tmp/__go_gopher_test_fs/bar", barData, os.FileMode(0644))
	if err != nil {
		t.Fatal("ioutil.WriteFile() failed:", err)
	}

	err = ioutil.WriteFile("/tmp/__go_gopher_test_fs/foo/bar", fooData, os.FileMode(0644))
	if err != nil {
		t.Fatal("ioutil.WriteFile() failed:", err)
	}

	handler := FileServer("localhost:7070", http.Dir("/tmp/__go_gopher_test_fs"))

	builder := strings.Builder{}
	req := &Request{Path: "/"}
	req = req.WithContext(context.WithValue(req.Context(), ServerContextKey, &Server{}))
	handler.ServeGopher(&builder, req)
	items := NewItemReader(strings.NewReader(builder.String()))
	i, err := items.Next()
	if err != nil {
		t.Error("Next() failed:", err)
	} else {
		testItem(i, ItemFile, "bar", "localhost:7070", "/bar")
	}
	i, err = items.Next()
	if err != nil {
		t.Error("Next() failed:", err)
	} else {
		testItem(i, ItemDirectory, "foo", "localhost:7070", "/foo")
	}

	builder.Reset()
	req = &Request{Path: "/bar"}
	req = req.WithContext(context.WithValue(req.Context(), ServerContextKey, &Server{}))
	handler.ServeGopher(&builder, req)
	if builder.String() != string(barData) {
		t.Errorf("FileServer() failed: builder.String() != \"hello, world!\\r\\n.\\r\\n\" (%s)", builder.String())
	}

	builder.Reset()
	req = &Request{Path: "/foo"}
	req = req.WithContext(context.WithValue(req.Context(), ServerContextKey, &Server{}))
	handler.ServeGopher(&builder, req)
	items = NewItemReader(strings.NewReader(builder.String()))
	i, err = items.Next()
	if err != nil {
		t.Error("Next() failed:", err)
	} else {
		testItem(i, ItemFile, "bar", "localhost:7070", "/foo/bar")
	}

	builder.Reset()
	req = &Request{Path: "/foo/bar"}
	req = req.WithContext(context.WithValue(req.Context(), ServerContextKey, &Server{}))
	handler.ServeGopher(&builder, req)
	if builder.String() != string(fooData) {
		t.Errorf("FileServer() failed: builder.String() != \"goodbye, world!\\r\\n.\\r\\n\" (%s)", builder.String())
	}
}
