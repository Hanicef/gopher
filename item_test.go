package gopher

import (
	"io"
	"strings"
	"testing"
)

const TestString = "iHello\tnil\tlocalhost\t7070\r\n" +
	"iWorld\tnil\tlocalhost\t7070\r\n" +
	"i\tnil\tlocalhost\t7070\r\n" +
	"1Go to gopherproject\t/\tgopherproject.org\t70\r\n" +
	".\r\n"

const TestInvalid = "hello, world!\r\n"

func TestItemReader(t *testing.T) {
	var testItem = func (item *Item, typ byte, name, host, path string) {
		if item.Type != typ {
			t.Errorf("List() failed: item.Type != typ (%c != %c)", item.Type, typ)
		}
		if item.Name != name {
			t.Errorf("List() failed: item.Name != name (%s != %s)", item.Name, name)
		}
		if item.Host != host {
			t.Errorf("List() failed: item.Host != host (%s != %s)", item.Host, host)
		}
		if item.Path != path {
			t.Errorf("List() failed: item.Path != Path (%s != %s)", item.Path, path)
		}
	}

	items := NewItemReader(strings.NewReader(TestString))

	i, err := items.Next()
	if err != nil {
		t.Fatal("Next() failed:", err)
	}
	testItem(i, ItemInformation, "Hello", "localhost:7070", "nil")

	i, err = items.Next()
	if err != nil {
		t.Fatal("Next() failed:", err)
	}
	testItem(i, ItemInformation, "World", "localhost:7070", "nil")

	i, err = items.Next()
	if err != nil {
		t.Fatal("Next() failed:", err)
	}
	testItem(i, ItemInformation, "", "localhost:7070", "nil")

	i, err = items.Next()
	if err != nil {
		t.Fatal("Next() failed:", err)
	}
	testItem(i, ItemDirectory, "Go to gopherproject", "gopherproject.org:70", "/")

	i, err = items.Next()
	if err != io.EOF {
		t.Fatal("Next() failed:", err)
	}

	items = NewItemReader(strings.NewReader(TestInvalid))
	if _, err = items.Next(); err != ErrMalformattedItem {
		if err == nil {
			t.Fatal("Next() incorrectly succeeded")
		} else {
			t.Fatal("Next() failed:", err)
		}
	}
	if items.ErrorLine() != TestInvalid {
		t.Errorf("ErrorLine() failed: items.ErrorLine() != TestInvalid (%s)", items.ErrorLine())
	}
}
