package gopher

import (
	"io"
	"io/ioutil"
	"testing"
)

const Response = "hello, world!"

type TestHandler int

func (_ TestHandler) ServeGopher(writer io.Writer, req *Request) {
	writer.Write([]byte(Response))
}

func TestFetch(t *testing.T) {
	server := Server{
		Addr: "localhost:7070",
		Handler: TestHandler(0),
	}
	go server.ListenAndServe()
	defer server.Shutdown()

	reader, err := Fetch("localhost:7070", "/")
	if err != nil {
		t.Error("Fetch() failed: ", err.Error())
		return
	}
	defer reader.Close()
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Error("Fetch() failed: ", err.Error())
	} else if string(data) != Response {
		t.Errorf("Fetch() failed: string(data) != Response (%s)", string(data))
	}
}
