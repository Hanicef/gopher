package gopher

import (
	"io"
	"strings"
)

type pat struct {
	h   Handler
	hr  Handler
	pat string
}

// ServeMux is a Gopher request multiplexer. It matches the path of each
// incoming request against a list of registered patterns and calls the handler
// for the pattern that most closely matches the path.
//
// Patterns are fixed, rooted paths, like "/index", or rooted subtrees, like
// "/images/" (note the trailing slash). Longer patterns take precendence over
// shorter ones, so that if there are handlers registered on both "/images/"
// and "/images/thumbnails/", the latter handler will be called for paths
// beginning with "/images/thumbnails/" and the former will be called for any
// other paths in the "/images/" subtree.
//
// Note that since a pattern ending in a slash names a rooted subtree, the
// pattern "/" matches all paths not matched by other registered patterns, not
// just the path with Path == "/".
//
// If a subtree has been registered and a request is received naming the
// subtree root without its trailing slash, ServeMux redirects that request to
// the subtree root (adding the trailing slash). This behaviour can be
// overridden with a seperate registration for the path without the trailing
// slash. For example, registering "/images/" causes ServeMux to redirect a
// request for "/images" to "/images/", unless "/images" has been registered
// seperately.
//
// Patterns may optionally skip the preceeding slash, allowing special paths
// like "URL:http://example.com/" to be registered. Patterns without the
// preceeding slash is matched seperately, for example, the pattern "/foo" will
// not match the path "foo", regardless if the pattern "foo" is registered or
// not.
type ServeMux struct {
	es []pat
}

// DefaultServeMux is the default ServeMux used by Serve.
var DefaultServeMux = ServeMux{}

// Handle registers the handler for the given pattern. If a handler already
// exists for pattern, Handle panics.
func (mux *ServeMux) Handle(pattern string, handler Handler) {
	recurse := pattern[len(pattern)-1] == '/'
	if recurse {
		pattern = pattern[:len(pattern)-1]
	}

	var l, r = 0, len(mux.es) - 1
	for l <= r {
		m := (l + r) / 2
		if len(mux.es[m].pat) > len(pattern) {
			l = m + 1
		} else if len(mux.es[m].pat) < len(pattern) {
			r = m - 1
		} else if mux.es[m].pat == pattern {
			if recurse {
				if mux.es[m].hr != nil {
					panic("pattern " + pattern + "/ is already registered")
				}
				mux.es[m].hr = handler
			} else {
				if mux.es[m].h != nil {
					panic("pattern " + pattern + " is already registered")
				}
				mux.es[m].h = handler
			}
			return
		} else {
			l = m
			break
		}
	}

	mux.es = append(mux.es, pat{})
	for i := len(mux.es) - 1; i > l; i-- {
		mux.es[i] = mux.es[i - 1]
	}
	mux.es[l].pat = pattern
	if recurse {
		mux.es[l].hr = handler
		mux.es[l].h = nil
	} else {
		mux.es[l].hr = nil
		mux.es[l].h = handler
	}
}

// HandleFunc registers the handler function for the given pattern.
func (mux *ServeMux) HandleFunc(pattern string, handler func (io.Writer, *Request)) {
	mux.Handle(pattern, HandlerFunc(handler))
}

// Handler returns the handler to use for the given path and the registered
// pattern that matches the given path.
//
// If there is no registered handler that matches path, Handler returns nil and
// an empty pattern.
func (mux *ServeMux) Handler(path string) (Handler, string) {
	if n := strings.Index(path, "?"); n != -1 {
		path = path[:n]
	}
	for _, m := range mux.es {
		if m.pat == path {
			if m.h != nil {
				return m.h, m.pat
			} else {
				return m.hr, m.pat + "/"
			}
		} else if m.hr != nil && strings.HasPrefix(path, m.pat + "/") {
			return m.hr, m.pat + "/"
		}
	}
	return nil, ""
}

// ServeGopher dispatches the request to the handler whose pattern most closely
// matches the requested path.
func (mux *ServeMux) ServeGopher(writer io.Writer, req *Request) {
	h, _ := mux.Handler(req.Path)
	if h == nil {
		writer.Write([]byte(".\r\n"))
		return
	}
	h.ServeGopher(writer, req)
}

// Handle registers the handler for the given pattern in the DefaultServeMux.
// The documentation for ServeMux explains how patterns are matched.
func Handle(pattern string, handler Handler) {
	DefaultServeMux.Handle(pattern, handler)
}

// Handle registers the handler function for the given pattern in the
// DefaultServeMux. The documentation for ServeMux explains how patterns are
// matched.
func HandleFunc(pattern string, handler func (io.Writer, *Request)) {
	DefaultServeMux.HandleFunc(pattern, handler)
}
