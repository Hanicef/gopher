package gopher

import (
	"context"
	"errors"
	"io"
	"log"
	"net"
	"sync"
	"time"
)

var (
	// ServerContextKey is a context key. It can be used in Gopher handlers
	// with Context.Value to access the server that started the handler.
	// The associated value will be of type *Server.
	ServerContextKey = &contextKey{"gopher-server"}

	// LocalAddrContextKey is a context key. It can be used in Gopher
	// handlers with Context.Value to access the local address the
	// connection arrived on. The associated value will be of type
	// net.Addr.
	LocalAddrContextKey = &contextKey{"local-addr"}
)

type contextKey struct {
	name string
}

type Server struct {
	// Addr specifies the TCP address for the server to listen on, in the
	// form "host:port". If empty, ":gopher" (port 70) is used.
	// See net.Dial for details of the address format.
	Addr string

	// Handler specifies the handler to invoke for each client. If nil,
	// DefaultServeMux is used.
	Handler Handler

	// ReadTimeout sets the maximum duration for reading the requested
	// path. A value of zero means no timeout.
	ReadTimeout time.Duration

	// WriteTimeout sets the maximum duration before timing out the writes
	// of the request. A value of zero means no timeout.
	WriteTimeout time.Duration

	// MaxPathBytes sets the maximum amount of bytes the server is willing
	// to read before returning an error. If zero, DefaultMaxPathBytes will
	// be used.
	MaxPathBytes int

	// ErrorLog specifies an optional logger for errors accepting
	// connections, unexpected behaviour from handlers, and underlying
	// FileSystem errors.
	// If nil, logging is done via the log package's standard logger.
	ErrorLog *log.Logger

	// BaseContext optionally specifies a function that returns the base
	// context for incoming requests on this server. The provided Listener
	// is the specific Listener that's about to start accepting requests.
	// If nil, the default context will be context.Background().
	// If non-nil, it must return a non-nil context.
	BaseContext func (net.Listener) context.Context

	// ConnContext optionally specifies a function that modifies the
	// context used for a new connection. The provided context is derived
	// from the base context and has a ServerContextKey value.
	ConnContext func (context.Context, net.Conn) context.Context

	listener net.Listener
	activeConn []net.Conn
	connMutex sync.Mutex
	connGroup sync.WaitGroup
	closeChan chan bool
}

type Request struct {
	// Path specifies the requested path. It is unmodified from the client
	// and is up to the handler to interpret, as there is no standardized
	// format for Gopher requests.
	Path string

	// RemoteAddr allows Gopher servers and other software to record the
	// network address that sent the request, usually for logging. This is
	// set to an "IP:port" address before invoking a handler.
	RemoteAddr string

	ctx context.Context
}

// WithContext returns a copy of req with its context changed to ctx. The
// provided ctx must not be nil.
func (req *Request) WithContext(ctx context.Context) *Request {
	if ctx == nil {
		panic("ctx is nil")
	}
	r := new(Request)
	r.ctx = ctx
	r.Path = req.Path
	r.RemoteAddr = req.RemoteAddr
	return r
}

// Context returns the request's context. To change the context, use
// WithContext.
//
// The returned context is always non-nil; it defaults to context.Background.
//
// The context is canceled when the ServeGopher method returns.
func (req *Request) Context() context.Context {
	if req.ctx == nil {
		req.ctx = context.Background()
	}
	return req.ctx
}

// DefaultMaxPathBytes is the default maximum length of the requested path if
// none is specified in Server.MaxPathBytes.
const DefaultMaxPathBytes = 256

func writeError(w io.Writer, err error) {
	w.Write([]byte("3error: " + err.Error() + "\tnil\tnil\tnil\r\n.\r\n"))
}

func (srv *Server) log(v ...interface{}) {
	if srv.ErrorLog != nil {
		srv.ErrorLog.Print(v...)
	} else {
		log.Print(v...)
	}
}

var ErrServerClosed = errors.New("gopher: Server closed")

// Serve accepts incoming connections from l, creating a new service goroutine
// for each accepted connection. The service goroutines read the requested path
// and calls srv.Handler to reply to them.
//
// Serve always returns a non-nil error and closes l. After Shutdown or Close,
// the returned error is ErrServerClosed.
func (srv *Server) Serve(l net.Listener) error {
	if srv.listener != nil {
		return ErrServerClosed
	}
	if srv.MaxPathBytes <= 0 {
		srv.MaxPathBytes = DefaultMaxPathBytes
	}
	baseCtx := context.Background()
	if srv.BaseContext != nil {
		baseCtx = srv.BaseContext(l)
		if baseCtx == nil {
			panic("BaseContext returned nil")
		}
	}
	baseCtx = context.WithValue(baseCtx, ServerContextKey, srv)

	h := srv.Handler
	if h == nil {
		h = &DefaultServeMux
	}

	srv.closeChan = make(chan bool, 1)
	srv.listener = l
	for {
		conn, err := l.Accept()
		if err != nil {
			select {
			case <-srv.closeChan:
				return ErrServerClosed
			default:
				srv.log(err)
				continue
			}
		}

		go func (conn net.Conn) {
			ctx := baseCtx
			if srv.ConnContext != nil {
				ctx = srv.ConnContext(baseCtx, conn)
				if ctx == nil {
					panic("ConnContext returned nil")
				}
			}

			var cancel context.CancelFunc
			ctx = context.WithValue(ctx, LocalAddrContextKey, conn.LocalAddr())
			ctx, cancel = context.WithCancel(ctx)

			srv.connGroup.Add(1)
			srv.connMutex.Lock()
			var index = -1
			for i := range srv.activeConn {
				if srv.activeConn[i] == nil {
					srv.activeConn[i] = conn
					index = i
					break
				}
			}
			if index == -1 {
				srv.activeConn = append(srv.activeConn, conn)
				index = len(srv.activeConn) - 1
			}
			srv.connMutex.Unlock()

			defer func () {
				if r := recover(); r != nil {
					srv.log(r)
				}
				cancel()
				err := conn.Close()
				if err != nil {
					srv.log(err.Error())
				}
				srv.activeConn[index] = nil
				srv.connGroup.Done()
			}()

			if srv.ReadTimeout != time.Duration(0) {
				err := conn.SetReadDeadline(time.Now().Add(srv.ReadTimeout))
				if err != nil {
					srv.log(err.Error())
					return
				}
			}

			line, err := readLine(conn, srv.MaxPathBytes)
			if err != nil {
				if err == errLineTooLong {
					writeError(conn, err)
				} else if err != io.EOF {
					srv.log(err.Error())
				}
				return
			}

			if srv.WriteTimeout != time.Duration(0) {
				err := conn.SetWriteDeadline(time.Now().Add(srv.WriteTimeout))
				if err != nil {
					srv.log(err.Error())
					return
				}
			}
			h.ServeGopher(conn, &Request{
				Path: line,
				RemoteAddr: conn.RemoteAddr().String(),
				ctx: ctx,
			})
		}(conn)
	}
}

// ListenAndServe listens on the TCP network address srv.Addr and then calls
// Serve to handle requests on incoming connections.
//
// If srv.Addr is blank, ":gopher" is used.
//
// Serve always returns a non-nil error and closes the underlying net.Listener.
// After Shutdown or Close, the returned error is ErrServerClosed.
func (srv *Server) ListenAndServe() error {
	if srv.listener != nil {
		return ErrServerClosed
	}
	if srv.Addr == "" {
		srv.Addr = ":70"
	}
	l, err := net.Listen("tcp", srv.Addr)
	if err != nil {
		return err
	}
	return srv.Serve(l)
}

// Close immediately closes the net.Listener as well as all connections. For a
// graceful shutdown, use Shutdown.
//
// Close returns any error returned from closing the underlying Listener.
// Once Shutdown has been called on a server, it may not be reused; future
// calls to methods like Serve will return ErrServerClosed.
func (srv *Server) Close() error {
	srv.closeChan <- true
	err := srv.listener.Close()
	for _, conn := range srv.activeConn {
		if conn != nil {
			conn.Close()
		}
	}
	return err
}

// Shutdown gracefully shuts down the server without interrupting any active
// connections. This works by first closing the corresponding net.Listener,
// then waiting indefinitely for all active connections to close.
//
// When Shutdown is called, Serve and ListenAndServe immediately return
// ErrServerClosed. Make sure the program doesn't exit and waits instead for
// Shutdown to return.
//
// Shutdown returns any error returned from closing the underlying Listener.
// Once Shutdown has been called on a server, it may not be reused; future
// calls to methods like Serve will return ErrServerClosed.
func (srv *Server) Shutdown() error {
	srv.closeChan <- false
	err := srv.listener.Close()
	srv.connGroup.Wait()
	return err
}

// Serve accepts incoming connections from l, creating a new service goroutine
// for each accepted connection. The service goroutines read the requested path
// and calls srv.Handler to reply to them.
//
// Serve always returns a non-nil error.
func Serve(l net.Listener, handler Handler) {
	srv := &Server{
		Handler: handler,
	}
	srv.Serve(l)
}

// ListenAndServe listens on the TCP network address addr and then calls Serve
// with handler to handle requests on incoming connections.
//
// If addr is blank, ":gopher" is used.
//
// If handler is nil, DefaultServeMux is used.
//
// Serve always returns a non-nil error.
func ListenAndServe(addr string, handler Handler) {
	srv := &Server{
		Addr: addr,
		Handler: handler,
	}
	srv.ListenAndServe()
}
